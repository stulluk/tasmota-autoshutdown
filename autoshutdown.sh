#!/usr/bin/env bash

#set -euo pipefail

#set -x

function print_usage()
{
	printf "$(basename $0) device_IP_to_be_checked device_PORT_to_be_checked shutdown_device_IP\n"
}


if [ $# -ne 3 ]
then
	print_usage
	exit
fi

deviceIPtoBeChecked=${1}
devicePORTtoBeChecked=${2}

shutdownDeviceIP=${3}

isDeviceAliveCheckDuration=600
doubleCheckDuration=60
shutdownDevicePowerCheckDuration=600


function shutdown_func(){

	printf "Shutting down ${1} \n"
	curl -s "http://${1}/cm?cmnd=Power%20OFF"
}

#Main thread
while [ 1 ]
do

	#Check if $deviceIPtoBeChecked is alive

	isAlive=$(nc -z -w 1 ${deviceIPtoBeChecked} ${devicePORTtoBeChecked} && echo "0" || echo "1")
	#printf "isAlive is ${isAlive}\n"

	if [ ${isAlive} -eq "0" ]
	then
		printf "${deviceIPtoBeChecked} is Alive, will check after ${isDeviceAliveCheckDuration} seconds!!\n"
		#Continue main thread
		sleep ${isDeviceAliveCheckDuration}
	else
		printf "${deviceIPtoBeChecked} is not alive!!\n"
		printf "Sleeping for ${doubleCheckDuration} seconds and check if still not alive!!\n"
		#Check again after ${doubleCheckDuration}  seconds
		sleep ${doubleCheckDuration} 
		isAlive=$(nc -z -w 1 ${deviceIPtoBeChecked} ${devicePORTtoBeChecked} && echo "0" || echo "1")
		#printf "isAlive is ${isAlive}\n"
		if [ ${isAlive} -eq "0" ]
		then
			printf "${deviceIPtoBeChecked} is Alive!!\n"
			#Continue main thread
			continue
		else
			printf "OK, ${deviceIPtoBeChecked} is still not alive, lets check if Power is already shut down...\n"
			isAlreadyPoweredOff=$(curl -s http://${shutdownDeviceIP}/cm?cmnd=POWER | jq -r .'POWER')
			if [ ${isAlreadyPoweredOff} == "OFF" ]
			then
				printf "Shutdown Device ${shutdownDeviceIP} is already powered OFF, waiting ${shutdownDevicePowerCheckDuration} seconds\n"
				sleep ${shutdownDevicePowerCheckDuration}
				continue
			else
				printf "Shutdown Device ${shutdownDeviceIP} is NOT powered OFF, shutting down... \n"
				shutdown_func ${shutdownDeviceIP}
			fi
		fi

	fi



done
